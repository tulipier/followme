# FollowMe

# Virtualenv

* You can create a virtual env with : `$ virtualenv venv`
* And activate it with : `$ source venv/bin/activate`
* To quit the virtualenv just do : `$ deactivate`

# Install dependencies

* System dependencies
  * python >= 3.6
  * pip (for python 3)
* `$ pip install -r requirements.txt`
*  Prepare database :
```bash
$ cd followme
$ python3 ./manage.py migrate
```
* Create superuser 
```bash
$ python3 ./manage.py createsuperuser
```

# Update

* `git pull`
* STOP the server if running
* `python3 ./manage.py migrate`
* Relaunch the server

# Run server 

```bash
$ cd followme
$ python3 ./manage.py runserver 0.0.0.0:8000
```



