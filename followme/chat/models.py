from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

# Auto update newmessages when SMS is saved (recv or seen)
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import RegexValidator

# from .consumers import WEB_SOCKETS_CONVERSATION, ConvConsumer
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from django.core.serializers import serialize

import datetime

# from .consumers import ConvConsumer

GENRES = (
    ('H', 'Masculin'),
    ('F', 'Féminin'),
    ('X', 'Autre')
)

DELAIS = (
    (0, 'En continue'),
    (10, '10 sec'),
    (20, '20 sec'),
    (30, '30 sec'),
    (45, '45 sec'),
    (60, '60 sec'),
    (300, '5 min')
)


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.__class__.objects.exclude(id=self.id).delete()
        super(SingletonModel, self).save(*args, **kwargs)

    @classmethod
    def load(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()


class GlobalConfig(SingletonModel):
    spectacle = models.OneToOneField('Spectacle', models.SET_NULL, blank=True,
                                     null=True)
    delaisFroid = models.PositiveSmallIntegerField("Délais d'inactivté",
                                                   default=10,
                                                   help_text="Délais en minutes")
    envoiSMS = models.BooleanField("Activer l'envoi des SMS", default=False)
    receptionSMS = models.BooleanField("Activer la réception des SMS",
                                       default=True)
    delaisReception = models.PositiveSmallIntegerField(
        "Délais entre les récupérations des SMS", default=10, choices=DELAIS)
    userAPI = models.CharField("Utilisateur pour l'API", max_length=255,
                               blank=True, null=True)
    cleAPI = models.CharField("Clé API", max_length=255, blank=True, null=True)
    vmn = PhoneNumberField("N° de téléphone virtuel", blank=True,
                           null=True, default=None, help_text="Au format +33..")

    @classmethod
    def get_or_create(cls):
        config = cls.objects.all()
        if len(config) == 0:
            config = (cls(),)
            config[0].save()
        return config[0]

    def __str__(self):
        return "Spectacle : {}".format(self.spectacle)

    class Meta:
        verbose_name = "0. Configuration globale"
        verbose_name_plural = "0. Configuration globale"


class Spectacle(models.Model):
    nom = models.CharField("Nom du spectacle", max_length=255, unique=True)
    datetime = models.DateTimeField("Date et heure du spectacle")
    texteurs = models.ManyToManyField('Texteur',
                                      verbose_name="Texteurs en jeu dans le spectacle")
    contacts = models.ManyToManyField('Contact',
                                      verbose_name="Contacts concernés par le spectacle",
                                      blank=True)
    groupes = models.ManyToManyField('Groupe',
                                     verbose_name="Groupes concernés par le spectacle",
                                     blank=True)
    modelsSMS = models.ManyToManyField('ModelSMS',
                                       verbose_name="Modeles de SMS concernés par le spectacle",
                                       blank=True)

    def actif(self):
        config = GlobalConfig.objects.all()
        if len(config) == 0:
            return "Non"
        if config[0].spectacle_id == self.id:
            return "Oui"
        else:
            return "Non"

    def __str__(self):
        return self.nom

    class Meta:
        verbose_name = "1. Spectacle"
        verbose_name_plural = "1. Spectacles"


class Texteur(models.Model):
    nom = models.CharField("Nom du texteur", max_length=255, unique=True,
                           validators=[
                               RegexValidator('^[a-zA-Z][a-zA-Z0-9_-]*$',
                                              message="Le nom du texteur ne "
                                                      "doit contenir que des "
                                                      "lettres, des chiffres "
                                                      "ou des tirets"),
                           ])

    def actif(self):
        config = GlobalConfig.objects.all()
        if len(config) == 0:
            return "Non"
        try:
            config[0].spectacle.texteurs.get(id=self.id)
            return "Oui"
        except Texteur.DoesNotExist:
            return "Non"

    def __str__(self):
        return self.nom

    class Meta:
        verbose_name = "2. Texteur"
        verbose_name_plural = "2. Texteurs"


class Contact(models.Model):
    numero = PhoneNumberField("N° de téléphone",
                              help_text="Au format +33..", unique=True)
    nom = models.CharField(max_length=255)
    prenom = models.CharField(max_length=255)
    genre = models.CharField(max_length=1, choices=GENRES)
    texteur = models.ForeignKey('Texteur', models.SET_NULL, blank=True,
                                null=True)
    lieu = models.CharField("Lieu de rendez-vous", max_length=255,
                            blank=True, null=True)
    newmessages = models.PositiveIntegerField("Messages(s) non lu(s)",
                                              default=0)
    lastmsgrecvtime = models.DateTimeField("Date du dernier message reçu",
                                           default=datetime.datetime(1970, 1, 1,
                                                                     0, 0, 0))
    lastmsgsendtime = models.DateTimeField("Date du dernier message envoyé",
                                           default=datetime.datetime(1970, 1, 1,
                                                                     0, 0, 0))

    def groupes(self):
        my_groupes = Groupe.objects.all().filter(membres=self.id)
        resp = ""
        for groupe in my_groupes:
            resp += "{}, ".format(groupe.nom)
        if resp != "":
            resp = resp[:-2]
        return resp

    def actif(self):
        config = GlobalConfig.objects.all()
        if len(config) == 0:
            return "Non"
        try:
            config[0].spectacle.contacts.get(id=self.id)
            return "Oui"
        except Contact.DoesNotExist:
            return "Non"

    def __str__(self):
        return "{}, {} - {} - ({})".format(self.nom.upper(), self.prenom,
                                           self.genre, self.numero)

    class Meta:
        verbose_name = "3. Contact"
        verbose_name_plural = "3. Contacts"


class Groupe(models.Model):
    nom = models.CharField("Nom du groupe", unique=True, max_length=255)
    membres = models.ManyToManyField('Contact',
                                     verbose_name="Membres du groupe")

    def nombre_de_membres(self):
        return str(self.membres.count())

    def actif(self):
        config = GlobalConfig.objects.all()
        if len(config) == 0:
            return "Non"
        try:
            config[0].spectacle.groupes.get(id=self.id)
            return "Oui"
        except Groupe.DoesNotExist:
            return "Non"

    def __str__(self):
        return self.nom

    class Meta:
        verbose_name = "4. Groupe"
        verbose_name_plural = "4. Groupes"


class SMS(models.Model):
    contact = models.ForeignKey('Contact', models.CASCADE)
    emitter = models.CharField(max_length=255, blank=True, null=True,
                               default="")
    datetime = models.DateTimeField()
    message = models.TextField()

    # seen = models.BooleanField(default=False)

    def __str__(self):
        return self.message


class ModelSMS(models.Model):
    nom = models.CharField("Nom du modele", max_length=255, unique=True)
    Hcontent = models.TextField("Message au masculin")
    Fcontent = models.TextField("Message au féminin")
    Acontent = models.TextField("Message pour le genre autre")
    conversation = models.BooleanField("Pour les conversation", default=True)
    groupes = models.BooleanField("Pour les groupes", default=True)

    def actif(self):
        config = GlobalConfig.objects.all()
        if len(config) == 0:
            return "Non"
        try:
            config[0].spectacle.modelsSMS.get(id=self.id)
            return "Oui"
        except ModelSMS.DoesNotExist:
            return "Non"

    def __str__(self):
        return self.nom

    class Meta:
        verbose_name = "5. Modele de SMS"
        verbose_name_plural = "5. Modeles de SMS"


@receiver(post_save, sender=SMS)
def sms_is_saved(sender, instance, created, update_fields=None, **kwargs):
    print("save sms : createad ? : {}".format(created))
    if created and instance.contact.texteur is not None:
        texteur = instance.contact.texteur.nom
        # Broadcast newmessage
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(texteur, {
            'type': 'broadcast_message',
            'action': 'signal_new_sms',
            'data': (instance.contact.id, serialize('json', [instance, ]))
        })
        print("signal new sms to group {}".format(texteur))
        # Update newmessages and lastmsgtime on contact
        if instance.emitter == "":
            instance.contact.newmessages += 1
            instance.contact.lastmsgrecvtime = instance.datetime
        else:
            instance.contact.lastmsgsendtime = instance.datetime
        instance.contact.save()


@receiver(post_save, sender=GlobalConfig)
def globalconfig_is_saved(sender, instance, **kwargs):
    from .apps import BACKGROUND_THREAD
    if BACKGROUND_THREAD is None:
        print("Background thread not inited (globalconfig saved)")
        return
    if BACKGROUND_THREAD.is_alive():
        BACKGROUND_THREAD.set_conf(instance.delaisReception,
                                   instance.userAPI,
                                   instance.cleAPI,
                                   None if instance.vmn == "" else instance.vmn)
    else:
        print("Background thread not started (globalconfig saved)")
