from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.login, name='index'),
    url(r'^conversation$', views.conversation),
    url(r'^dashboard$', views.dashboard),
    url(r'^swap', views.swap),
    url(r'^eject', views.eject),
    url(r'^export', views.export),
    url(r'^groupes', views.groupes),
    url(r'^logout$', views.logout),
    # url(r'^(?P<room_name>[^/]+)/$', views.room, name='room'),
]
