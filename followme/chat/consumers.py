# -*- coding: utf-8 -*-

from channels.generic.websocket import WebsocketConsumer
from django.core.serializers import serialize
from asgiref.sync import async_to_sync
from . import models, allmysms

from .apps import replace_token_in_sms

import json
import datetime
import locale

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF8')

# WEB_SOCKETS_CONVERSATION = dict()

class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        self.send(text_data=json.dumps({
            'message': message
        }))


class ConvConsumer(WebsocketConsumer):

    def connect(self):
        print("Init socket")
        self.texteur = self.scope['url_route']['kwargs']['texteur_name']
        self.accept()
        print("Add channel_name {} to group {}".format(self.channel_name, self.texteur))
        async_to_sync(self.channel_layer.group_add)(self.texteur, self.channel_name)
        print("OK socker")

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(self.texteur, self.channel_name)
        print("Remove channel_name {} from group {}".format(self.channel_name, self.texteur))

    def recv_getcontactinfo(self, data):
        contact_id = data['contact_id']
        print("get contactinfo on {}".format(contact_id))
        config = models.GlobalConfig.objects.all()[0]
        contact = config.spectacle.contacts.get(id=contact_id)
        messages = models.SMS.objects.filter(contact_id=contact_id)
        json_contact = serialize('json', [contact, ])
        json_messages = serialize('json', messages)
        # response = {
        #     'type':  "contactinfo",
        #     'data': {
        #         'contact': json_contact,
        #         'messages': json_messages
        #     }
        # }
        # self.send(text_data=json.dumps(response))
        self.send(text_data=
                  '{"type": "contactinfo","data": {"contact": '+json_contact+', "messages": '+json_messages+'}}'
                  )
        print("ok contact info")

    def recv_sendmsg(self, data):
        message = data['message']
        contact_id = data['contact_id']
        contact = models.Contact.objects.get(id=contact_id)
        config = models.GlobalConfig.objects.all()[0]
        message = replace_token_in_sms(message, contact)
        vmn = None if config.vmn == "" else config.vmn
        if config.envoiSMS is False or \
                allmysms.send_sms(config.userAPI, config.cleAPI,
                                  [contact.numero, ], message, vmn=vmn) is True:
            models.SMS(
                contact_id=contact_id,
                emitter=self.texteur,
                datetime=datetime.datetime.now(),
                message=message
            ).save()

    def recv_getcontactlist(self, data):
        config = models.GlobalConfig.objects.all()[0]
        contacts = config.spectacle.contacts.\
            filter(texteur_id=data['texteur_id']).\
            order_by('lastmsgrecvtime')
        print("{}".format(contacts))
        json_contacts = serialize('json', contacts)
        txt_data = '{"type": "contactlist","data": {"contact_list": '+json_contacts+'}}'
        # print(txt_data)
        self.send(text_data=txt_data)

    def recv_msgseen(self, data):
        contact_id = data['contact_id']
        contact = models.Contact.objects.get(id=contact_id)
        contact.newmessages = 0
        contact.save()

    def send_newmsg(self, data):
        print("send_newsms")
        contact_id, json_sms = data
        # json_sms = serialize('json', [sms, ])
        self.send(text_data='{"type": "newmsg","data": {"contact_id": '+
                            str(contact_id) + ', "sms": ' + json_sms + '}}')

    def broadcast_message(self, event):
        print("get Group signal : {}".format(event))
        if event['action'] == "signal_new_sms":
            self.send_newmsg(event['data'])
        elif event['action'] == "signal_contact_change":
            self.recv_getcontactlist(event['data'])

    def receive(self, text_data=None, *args):
        if text_data is None:
            print("Drop message (text)")
        message = json.loads(text_data)
        if 'type' not in message.keys() or 'data' not in message.keys():
            print("Drop message (format) : {}".format(text_data))
            return

        if message['type'] == "getcontactlist":
            return self.recv_getcontactlist(message['data'])
        elif message['type'] == "sendmsg":
            print("Recv sendmsg at : {}".format(datetime.datetime.now()))
            return self.recv_sendmsg(message['data'])
        elif message['type'] == "getcontactinfo":
            return self.recv_getcontactinfo(message['data'])
        elif message['type'] == "msgseen":
            return self.recv_msgseen(message['data'])
        else:
            print("Drop message (type) : {}".format(text_data))

