# Generated by Django 2.1.5 on 2019-02-20 16:28

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0015_contact_lieu'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contact',
            name='lastmsgtime',
        ),
        migrations.AddField(
            model_name='contact',
            name='lastmsgrecvtime',
            field=models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0), verbose_name='Date du dernier message reçu'),
        ),
        migrations.AddField(
            model_name='contact',
            name='lastmsgsendtime',
            field=models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0), verbose_name='Date du dernier message envoyé'),
        ),
    ]
