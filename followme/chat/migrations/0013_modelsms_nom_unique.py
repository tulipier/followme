from django.db import migrations, models

import uuid


def create_uuid(apps, schema_editor):
    ModelSMS = apps.get_model('chat', 'ModelSMS')
    print("\n\n\tModel : {}\n\n".format(ModelSMS))
    for model in ModelSMS.objects.all():
        model.nom = uuid.uuid4()
        model.save(update_fields=['nom'])
    print("\nOK create uuid\n")


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0012_modelsms_nom'),
    ]

    operations = [
        migrations.RunPython(create_uuid),
        migrations.AlterField(
            model_name='modelsms',
            name='nom',
            field=models.CharField(unique=True, max_length=255,
                                   verbose_name='Nom du modele'),
            preserve_default=False,
        )
    ]
