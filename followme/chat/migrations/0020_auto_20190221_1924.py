# Generated by Django 2.1.5 on 2019-02-21 18:24

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0019_globalconfig_delaisfroid'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contact',
            options={'verbose_name': '3. Contact', 'verbose_name_plural': '3. Contacts'},
        ),
        migrations.AlterModelOptions(
            name='globalconfig',
            options={'verbose_name': '0. Configuration globale', 'verbose_name_plural': '0. Configuration globale'},
        ),
        migrations.AlterModelOptions(
            name='groupe',
            options={'verbose_name': '4. Groupe', 'verbose_name_plural': '4. Groupes'},
        ),
        migrations.AlterModelOptions(
            name='modelsms',
            options={'verbose_name': '5. Modele de SMS', 'verbose_name_plural': '5. Modeles de SMS'},
        ),
        migrations.AlterModelOptions(
            name='spectacle',
            options={'verbose_name': '1. Spectacle', 'verbose_name_plural': '1. Spectacles'},
        ),
        migrations.AlterModelOptions(
            name='texteur',
            options={'verbose_name': '2. Texteur', 'verbose_name_plural': '2. Texteurs'},
        ),
        migrations.AddField(
            model_name='spectacle',
            name='datetime',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date et heure du spectacle'),
            preserve_default=False,
        ),
    ]
