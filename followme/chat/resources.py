# -*- coding: utf-8 -*-

from import_export import resources
from .models import Contact


class ContactResource(resources.ModelResource):
    class Meta:
        model = Contact
        fields = ['nom', 'prenom', 'genre', 'numero', 'lieu']
        import_id_fields = ['numero']

