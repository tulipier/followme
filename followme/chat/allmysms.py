# -*- coding: utf-8 -*-

import json
import datetime
import urllib.request
import urllib.parse

from asgiref.sync import async_to_sync
from .models import SMS, Contact, GlobalConfig, Groupe
from .apps import swap_token_by_param_in_sms, replace_token_in_sms

API_URL = "https://api.allmysms.com/http/9.0"

from followme.settings import GSM7_UNAUTHORIZED


def sms_recv(numero, message):
    try:
        contact = Contact.objects.get(numero="+" + numero)
    except Contact.DoesNotExist:
        print(
            "Get SMS from an unknown contact : {}\n{}".format(numero, message))
        contact = Contact(
            numero="+"+numero,
            nom="???",
            prenom="???",
            genre="X"
        )
        config = GlobalConfig.get_or_create()
        contact.save()
        if config.spectacle is not None:
            config.spectacle.contacts.add(contact)
            config.spectacle.save()
            if config.spectacle.texteurs.count() > 0:
                contact.texteur_id = config.spectacle.texteurs.all()[0].id
        contact.save()
    message = message.replace("\\\"", "\"")
    message = message.replace("\\\'", "\'")
    SMS(
        contact_id=contact.id,
        datetime=datetime.datetime.now(),
        message=message
    ).save()


def send_request(user, apikey, url, data):
    data['login'] = user
    data['apiKey'] = apikey
    data_parsed = urllib.parse.urlencode(data).encode()
    req = urllib.request.Request("{}/{}/".format(API_URL, url),
                                 data=data_parsed)
    if url == "sendSms":
        print("Sending request for sendSms at : {}".format(datetime.datetime.now()))
    try:
        response = urllib.request.urlopen(req)
    except urllib.request.URLError as e:
        print("Fail to send request : {} with {}".format(url, data))
        print("--ERROR--\n{}".format(e))
        return False
    if url == "sendSms":
        print("Ok sending request for sendSms at : {}".format(datetime.datetime.now()))
    return response


def get_all_sms(user, apikey, vmn=None):
    if vmn is None:
        return get_all_sms_basic(user, apikey)
    else:
        return get_all_sms_vmn(user, apikey, str(vmn).replace("+", ""))


def get_all_sms_vmn(user, apikey, vmn):
    resp = send_request(user, apikey, 'getvmnmo', {
        "vmn": vmn
    })
    if resp is False or resp.status != 200:
        print("Error in get all sms : {}".format(resp))
        return False
    messages = json.load(resp)
    print("get from all my sms PULLs VMN {} : {}".format(vmn, messages))
    if 'vmnmo' not in messages.keys() or len(messages['vmnmo']) == 0:
        # no new messages
        return False
    print("Get some SMS !")
    for sms in messages['vmnmo']:
        sms_recv(sms['msisdn'], sms['content'])


def get_all_sms_basic(user, apikey):
    resp = send_request(user, apikey, 'getPulls', dict())
    if resp is False or resp.status != 200:
        print("Error in get all sms : {}".format(resp))
        return False
    messages = json.load(resp)
    print("get from all my sms PULLs : {}".format(messages))
    if 'mos' not in messages.keys():
        # no new messages
        return False
    print("Get some SMS !")
    for sms in messages['mos']:
        sms_recv(sms['phoneNumber'], sms['message'])


def send_sms(user, apikey, numeros, message, params=None, vmn=None):
    if len(numeros) == 0:
        print("Empty numeros list")
        return True
    need_unicode = False
    if params is None:
        params = list()
    for char in GSM7_UNAUTHORIZED:
        if char in message:
            need_unicode = True
            print("Need unicode because of : {}".format(char))
            break
        if need_unicode:
            break
    sms = {
        "DATA": {
            "MESSAGE": message,
            "DYNAMIC": str(len(params)),
            # "CAMPAIGN_NAME": "followme",
            "SMS": []
        }
    }
    if vmn is not None:
        sms["DATA"]["TPOA"] = str(vmn).replace("+", "")
    else:
        print("Sending SMS without VMN ")
    for numero in numeros:
        data = {
            "MOBILEPHONE": str(numero)
        }
        for i in range(len(params)):
            data["PARAM_{}".format(i+1)] = params[i][str(numero)]
            if need_unicode is False:
                for char in GSM7_UNAUTHORIZED:
                    if char in params[i][str(numero)]:
                        need_unicode = True
                        print("Need unicode because of : {}".format(char))
                        break
                if need_unicode:
                    break
        sms["DATA"]["SMS"].append(data)
    if need_unicode:
        sms["DATA"]["CODING"] = "2"
    print("SMS dict to send : {}".format(sms))
    data = {
        "smsData": json.dumps(sms)
    }
    resp = send_request(user, apikey, 'sendSms', data)
    if resp is False or resp.status != 200:
        return False
    resp = json.load(resp)
    if resp["status"] != 100:
        print("Error during SMS sending : {} : {}".format(resp["status"],
                                                          resp["statusText"]))
        return False
    else:
        print("Sens SMS resp : {}".format(resp))
    return True


def _send_groupe_sms(user, apiKey, contacts, raw_message, dry_run=False, vmn=None):
    message, fields = swap_token_by_param_in_sms(raw_message)
    params = []
    for field in fields:
        data = dict()
        for contact in contacts:
            data[str(contact.numero)] = str(contact.__dict__[field])
        params.append(data)
    numeros = [c.numero for c in contacts]
    if dry_run or send_sms(user, apiKey, numeros, message, params, vmn) is True:
        for contact in contacts:
            SMS(
                contact_id=contact.id,
                datetime=datetime.datetime.now(),
                message=replace_token_in_sms(raw_message, contact),
                emitter="Follow Me"
            ).save()
        return True
    else:
        return False


def send_groupe_sms(user, apikey, groupe_id, Fcontent, Hcontent, Acontent,
                    dry_run=False, vmn=None):
    try:
        if groupe_id != "all":
            membres = Groupe.objects.get(id=groupe_id).membres.all()
        else:
            membres = GlobalConfig.get_or_create().spectacle.contacts.all()
    except Groupe.DoesNotExist:
        return -1
    resp = 0
    if _send_groupe_sms(user, apikey, membres.filter(genre='F'),
                        Fcontent, dry_run, vmn) is True:
        resp += 1
    if _send_groupe_sms(user, apikey, membres.filter(genre='H'),
                        Hcontent, dry_run, vmn) is True:
        resp += 1
    if _send_groupe_sms(user, apikey, membres.filter(genre='X'),
                        Acontent, dry_run, vmn) is True:
        resp += 1
    return resp


