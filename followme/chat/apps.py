from django.apps import AppConfig

import threading
import time
import atexit
import re


BACKGROUND_THREAD = None
CONTACT_FIELDS_RE = None


def replace_token_in_sms(message, contact):
    contact = contact.__dict__
    for field, regex in CONTACT_FIELDS_RE.items():
        message = regex.sub(str(contact[field]), message)
    return message


def swap_token_by_param_in_sms(message):
    i = 1
    fields = list()
    for field, regex in CONTACT_FIELDS_RE.items():
        tmp = regex.sub("#param_{}#".format(i), message)
        if tmp != message:
            fields.append(field)
            message = tmp
            i += 1
    return message, fields


class BackgroundThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self._should_stop = threading.Event()
        self._event = threading.Event()
        self.vmn = None
        self.delais = None
        self.user = None
        self.apikey = None

    def run(self):
        from . import allmysms
        atexit.register(self.stop)
        print("Start backround thread")
        while self._should_stop.is_set() is False:
            self._event.clear()
            t_start = time.time()
            # print("Thread, delais = {}".format(self.delais))
            try:
                allmysms.get_all_sms(self.user, self.apikey, self.vmn)
            except Exception as e:
                print("Thread runtime error : {}".format(e))
            self._event.wait(timeout=self.delais - time.time() + t_start+1)

    def set_conf(self, delais, user, apikey, vmn):
        self.delais = delais
        self.user = user
        self.apikey = apikey
        self.vmn = vmn
        self._event.set()

    def start_once(self, delais, user, apikey, vmn):
        if self.is_alive() is not True:
            if self._should_stop.is_set():
                # Thread have been already started
                global BACKGROUND_THREAD
                BACKGROUND_THREAD = BackgroundThread()
                BACKGROUND_THREAD.start_once(delais, user, apikey, vmn)
                return
            self.delais = delais
            self.user = user
            self.apikey = apikey
            self.vmn = vmn
            self.start()

    def stop(self):
        print("Stop background thread")
        self._event.set()
        self._should_stop.set()


class ChatConfig(AppConfig):
    name = 'chat'
    verbose_name = "Follow Me"

    def ready(self):
        from django.contrib import admin
        from followme.settings import ADMIN_ORDERING
        global BACKGROUND_THREAD, CONTACT_FIELDS_RE

        def get_app_list(self, request):
            app_dict = self._build_app_dict(request)
            for app_name, object_list in ADMIN_ORDERING:
                app = app_dict[app_name]
                app['models'].sort(
                    key=lambda x: object_list.index(x['object_name']))
                yield app

        BACKGROUND_THREAD = BackgroundThread()
        CONTACT_FIELDS_RE = dict()
        for field in ['nom', 'prenom', 'numero', 'lieu']:
            CONTACT_FIELDS_RE[field] = \
                re.compile("\${}".format(field))
        # admin.site.__class__.get_app_list = get_app_list

