# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils.safestring import mark_safe
#from django.http import JsonResponse
from django.core.serializers import serialize
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from followme.settings import FOLLOW_ME_VERSION, GSM7_UNAUTHORIZED

import urllib.parse
import json

from . import models

from .apps import BACKGROUND_THREAD
from .allmysms import send_groupe_sms

# Create your views here.


def unauth_redirect(request,msg=None):
    keys = request.session.flush()
    if msg is not None:
        msg = "Vous avez été déconnecté : {}".format(msg)
    return redirect("/", message=msg)


def logout(request):
    return unauth_redirect(request)


def login_section(function):
    def wrapper(request, *args, **kw):
        config = models.GlobalConfig.objects.all()[0]
        if config.receptionSMS:
            BACKGROUND_THREAD.start_once(config.delaisReception,
                                         config.userAPI,
                                         config.cleAPI,
                                         None if config.vmn == "" else config.vmn)
        spectacle = request.session.get('spectacle', False)
        if spectacle is False or str(config.spectacle) != spectacle:
            return unauth_redirect(request, "{} != {}".format(spectacle, config.spectacle))
        texteur = request.session.get('texteur', False)
        if texteur is False or not config.spectacle.texteurs.filter(nom=texteur).exists():
            return unauth_redirect(request, "texteur")
        # ok
        print("OK login section")
        data = dict()
        data["config"] = config
        data["gsm_unauthorized"] = GSM7_UNAUTHORIZED
        return function(request, config, data, *args, **kw)
    return wrapper


def can_display_message(function):
    def wrapper(request, config, data, *args, **kwargs):
        dispmsg = request.GET.get('dispmsg', False)
        if dispmsg is not False:
            data["disp_msg"] = {
                'msg': dispmsg,
                'type': request.GET.get('severity', 'error')
            }
        return function(request, config, data, *args, **kwargs)
    return wrapper


def index(request):
    return render(request, 'chat/index.html', {})


def login(request):
    config = None
    try:
        config = models.GlobalConfig.objects.all()[0]
    except IndexError:
        print("No global config ")
        return redirect("/admin/chat")
    if config.spectacle is None:
        print("No spectacle in current config")
        return redirect("/admin/chat")
    try:
        bdd_texteur = config.spectacle.texteurs.all()
    except AttributeError:
        print("No global config")
        return redirect("/admin/chat")
    data = dict()
    data["fm_version"] = FOLLOW_ME_VERSION
    message = request.GET.get('message', False)
    if message is not False:
        data["message"] = message
    elif request.session.get('texteur', False) is not False:
        return redirect('/dashboard')
    elif request.method == "POST":
        # Try to login
        texteur = request.POST.get('texteur', False)
        if texteur is not False:
            for t in bdd_texteur:
                print("texteur : {} // {}".format(texteur, t.nom))
                if texteur == t.nom:
                    # Trouvé !
                    request.session["texteur"] = t.nom
                    request.session["texteur_id"] = t.id
                    request.session["spectacle"] = config.spectacle.nom
                    return redirect('/dashboard')
            # Pas trouvé
            data["error"] = "Texteur invalide"
        else:
            data["error"] = "Requête invalide"
    data["spectacle"] = config.spectacle
    data["texteurs"] = bdd_texteur
    return render(request, 'chat/login.html', data)


@login_section
@can_display_message
def dashboard(request, config, data):
    data["title"] = "Tableau de bord"
    data["page"] = "dashboard"
    contacts = config.spectacle.contacts.filter(
        texteur_id=request.session.get('texteur_id'))
    data["contacts"] = contacts
    data["texteur_list"] = serialize('json', config.spectacle.texteurs.exclude(
        id=request.session.get('texteur_id')))
    return render(request, 'chat/dashboard.html', data)


@login_section
def export_txt(request, config, data):
    contact_id = request.GET.get('contact', False)
    if contact_id is False:
        print("ERROR in export : contact unspecified")
        return redirect('/dashboard?dispmsg=Aucun contact selectionné')
    try:
        contact = models.Contact.objects.get(id=contact_id)
    except models.Contact.DoesNotExist:
        print("ERROR in export : contact unfound id={}".format(contact_id))
        return redirect('/dashboard?dispmsg=Le contact selectionné est '
                        'introuvable')
    conv = ""
    messages = models.SMS.objects.filter(contact_id=contact_id).order_by('datetime')
    for sms in messages:
        sender = ""
        if sms.emitter == "":
            # from contact
            sender = "[{}]".format(contact.prenom)
        else:
            sender = "[Follow Me]"
        conv += "{} - {}\n{}\n\n".format(sender, sms.datetime, sms.message)

    resp = HttpResponse(conv, content_type="text/plain")
    resp['Content-Disposition'] = "attachement; filename=\"{}.txt\"".format(
        "conversation-{}_{}".format(
            contact.nom,
            contact.prenom
        )
    )
    return resp


@login_section
def export(request, config, data):
    contact_id = request.GET.get('contact', False)
    if contact_id is False:
        print("ERROR in export : contact unspecified")
        return redirect('/dashboard?dispmsg=Aucun contact selectionné')
    try:
        contact = models.Contact.objects.get(id=contact_id)
    except models.Contact.DoesNotExist:
        print("ERROR in export : contact unfound id={}".format(contact_id))
        return redirect('/dashboard?dispmsg=Le contact selectionné est '
                        'introuvable')
    conv = ""
    messages = models.SMS.objects.filter(contact_id=contact_id).order_by('datetime')
    # for sms in messages:
    #     sender = ""
    #     if sms.emitter == "":
    #         # from contact
    #         sender = "[{}]".format(contact.prenom)
    #     else:
    #         sender = "[Follow Me]"
    #     conv += "{} - {}\n{}\n\n".format(sender, sms.datetime, sms.message)
    data["messages"] = messages
    data["contact"] = contact
    return render(request, "chat/export.html", data)


@login_section
@can_display_message
def groupes(request, config, data):
    data["title"] = "Messages de groupe"
    data["page"] = "groupes"
    dest_groupe = request.POST.get('dest_groupe', None)
    if dest_groupe is not None:
        # SEND TO GROUPE !
        Fcontent = request.POST.get('Fcontent', '')
        Hcontent = request.POST.get('Hcontent', '')
        Acontent = request.POST.get('Acontent', '')
        vmn = None if config.vmn == "" else config.vmn
        resp = send_groupe_sms(config.userAPI, config.cleAPI, dest_groupe,
                               Fcontent, Hcontent, Acontent, not config.envoiSMS, vmn=vmn)
        if resp == 3:
            data["disp_msg"] = {
                "msg": "Le message a été correctement envoyé au groupe",
                "type": "success"
            }
        else:
            data["disp_msg"] = {
                "msg": "Une erreure est survenue lors de "
                       "l'envoi du message au groupe : {}".format(resp),
                "type": "error"
            }
        # To avoid (F5) = resend message without confirm
        return redirect("/groupes?groupe={}&dispmsg={msg}&severity={type}".
                        format(dest_groupe, **data['disp_msg']))
    list_groupes = config.spectacle.groupes.all()
    list_models = config.spectacle.modelsSMS.filter(groupes=True).order_by('nom')
    data["groupes"] = list_groupes
    data["models"] = list_models
    data["model_list"] = serialize('json', list_models)
    selected = request.GET.get('groupe', False)
    if selected is not False and selected != "all":
        try:
            data["current_groupe"] = list_groupes.get(id=selected)
        except models.Groupe.DoesNotExist:
            return redirect("/groupe?dispmsg=Le groupe demandé n'existe pas")
    else:
        data["current_groupe"] = {"id": "all", "nom": "TOUT LE MONDE"}
    return render(request, 'chat/groupes.html', data)


@login_section
@can_display_message
def conversation(request, config, data):
    data["title"] = "Conversation"
    data["page"] = "conversation"
    contact_id = request.GET.get('contact', False)
    if contact_id is not False:
        data["contact_id"] = contact_id
    data["texteur_list"] = serialize('json', config.spectacle.texteurs.exclude(
        id=request.session.get('texteur_id')))
    data["model_list"] = serialize('json', config.spectacle.modelsSMS.filter(
        conversation=True).order_by('nom'))
    return render(request, 'chat/conversation.html', data)


@login_section
def swap(request, config, data):
    contact_id = request.POST.get('contact-id', None)
    texteur_to_id = request.POST.get('texteur-to-id', None)
    return_to = "/" + request.POST.get('return-to', 'dashboard')
    try:
        texteur_to = models.Texteur.objects.get(id=texteur_to_id).nom
    except models.Texteur.DoesNotExist:
        print("ERROR in Swap : Texteur not exist")
        return redirect(return_to+'?dispmsg=Le texteur selectionné est '
                                  'introuvable')
    texteur_from_id = request.session.get('texteur_id', None)
    try:
        contact = models.Contact.objects.get(id=contact_id,
                                             texteur_id=texteur_from_id)
    except models.Contact.DoesNotExist:
        print("ERROR in Swap : contact not in texteur")
        return redirect(return_to+'dispmsg=Le contact selectionné est '
                                  'introuvable')
    contact.texteur_id = texteur_to_id
    contact.save()
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(texteur_to, {
        'type': 'broadcast_message',
        'action': 'signal_contact_change',
        'data': {'texteur_id': texteur_to_id}
    })
    print("signal contact change to group {}".format(texteur_to))
    return redirect(return_to)


@login_section
def eject(request, config, data):
    contact_id = request.GET.get('contact_id', None)
    return_to = "/" + request.GET.get('return_to', 'conversation')
    texteur_from_id = request.session.get('texteur_id', None)
    texteur_from = request.session.get('texteur', '')
    try:
        contact = config.spectacle.contacts.get(id=contact_id,
                                                texteur_id=texteur_from_id)
    except models.Contact.DoesNotExist:
        print("ERROR in Eject : contact not in texteur")
        return redirect(return_to+'dispmsg=Le contact selectionné est '
                                  'introuvable')
    # Eject contact from : SPECTACLE
    config.spectacle.contacts.remove(contact)
    config.spectacle.save()
    # Eject contact from : GROUPES
    for groupe in config.spectacle.groupes.all():
        groupe.membres.remove(contact)
        groupe.save()
    # Eject contact from : TEXTEUR
    contact.texteur = None
    contact.save()
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(texteur_from, {
        'type': 'broadcast_message',
        'action': 'signal_contact_change',
        'data': {'texteur_id': texteur_from_id}
    })
    print("signal contact change to group {}".format(texteur_from))
    return redirect(return_to)
