from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from chat.models import Spectacle, Texteur, Contact, GlobalConfig, \
    Groupe, ModelSMS
from chat.resources import ContactResource

admin.site.site_header = 'Administration de Follow Me'
admin.site.site_title = 'Administration de Follow Me'
admin.site.index_title = 'Administration de Follow Me'


class GroupeInline(admin.TabularInline):
    model = Groupe.membres.through


class IsActif(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = "Actif"

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'actif'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def choices(self, changelist):
        for lookup, title in self.lookup_choices:
            yield {
                'selected': self.value() == str(lookup),
                'query_string': changelist.get_query_string(
                    {self.parameter_name: lookup}),
                'display': title,
            }

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ("Tout", "Tout"),
            ("Oui", "Oui"),
            ("Non", "Non"),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() is None or self.value() == "Tout":
            return queryset
        actif = [o.id for o in queryset if o.actif() == self.value()]
        return queryset.filter(id__in=actif)


@admin.register(Contact)
class ContactAdmin(ImportExportModelAdmin):
    sortable_by = ('nom', 'prenom', 'numero', 'genre', 'texteur', 'lieu')
    ordering = ('-lastmsgrecvtime',)
    readonly_fields = ('lastmsgrecvtime', 'lastmsgsendtime', "newmessages")
    search_fields = ('nom', 'prenom', 'numero', 'texteur__nom', 'lieu')
    list_display = ('nom', 'prenom', 'genre', 'numero', 'texteur', 'lieu',
                    'groupes', 'actif')
    list_display_links = ('nom', 'prenom')
    # list_editable = ('texteur', )
    list_filter = ('genre', 'texteur', IsActif)
    resource_class = ContactResource
    inlines = [GroupeInline, ]

    import_template_name = 'admin/import_contact.html'
    add_form_template = "admin/spectacle_elem_addform.html"

    # def get_queryset(self, request):
    #     qs = super(ContactAdmin, self).get_queryset(request)
    #     # qs = qs.annotate(a_groupes="plop")
    #     return qs
    #
    # def a_groupes(self, obj):
    #     return obj.groupes()
    #
    # a_groupes.admin_order_field = 'groupes'

    @admin.options.csrf_protect_m
    def changelist_view(self, request, *args, **kwrags):
        actif = request.GET.get('actif', None)
        if actif is None:
            request.GET._mutable = True
            request.GET['actif'] = "Oui"
            request.GET._mutable = False
        return super(ImportExportModelAdmin, self).changelist_view(request,
                                                                   *args,
                                                                   **kwrags)

    def process_dataset(self, dataset, confirm_form, request, *args, **kwargs):
        resp = ImportExportModelAdmin.process_dataset(self, dataset,
                                                      confirm_form, request,
                                                      *args, **kwargs)
        if "add-to-spectacle" in request.POST and \
                request.POST['add-to-spectacle'] == "on":
            config = GlobalConfig.get_or_create()
            if config.spectacle is not None and config.spectacle.texteurs.count() > 0:
                print("Import on spectacle : {}".format(config.spectacle))
                print("\t imported data : \n{}".format(dataset))
                texteurs = config.spectacle.texteurs.all()
                n_contacts = len(dataset)
                i = n_contacts
                n = n_contacts / len(texteurs)
                for imported_contact in dataset:
                    try:
                        contact = Contact.objects.get(
                            numero=imported_contact[0])
                    except Contact.DoesNotExist:
                        print("Error during CSV import, "
                              "contact with numero {} DoesNotExist".format(
                            imported_contact[0]))
                    else:
                        config.spectacle.contacts.add(contact.id)
                        contact.texteur_id = texteurs[
                            int((n_contacts - i) / n)].id
                        contact.save()
                    i -= 1
                config.spectacle.save()
        return resp

    def response_add(self, request, obj, post_url_continue=None):
        if "add-to-spectacle" in request.POST and \
                request.POST['add-to-spectacle'] == "on":
            config = GlobalConfig.objects.all()
            if len(config) > 0 and config[0].spectacle is not None:
                config[0].spectacle.contacts.add(obj)
                config[0].spectacle.save()
        return super().response_add(request, obj)

    def view_on_site(self, obj):
        return "/conversation?contact={}".format(obj.id)


@admin.register(Groupe)
class GroupeAdmin(admin.ModelAdmin):
    sortable_by = ('actif', 'nom', 'nombre_de_membres')
    list_display = ('nom', 'nombre_de_membres', 'actif')
    filter_horizontal = ('membres',)
    search_fields = (
    'nom', 'membres__numero', 'membres__prenom', 'membres__nom')
    list_filter = (IsActif,)

    add_form_template = "admin/spectacle_elem_addform.html"

    @admin.options.csrf_protect_m
    def changelist_view(self, request, *args, **kwrags):
        actif = request.GET.get('actif', None)
        if actif is None:
            request.GET._mutable = True
            request.GET['actif'] = "Oui"
            request.GET._mutable = False
        return admin.ModelAdmin.changelist_view(self, request,
                                                *args,
                                                **kwrags)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        config = GlobalConfig.objects.all()
        if len(config) > 0 and config[0].spectacle is not None:
            if db_field.name == "membres":
                kwargs["queryset"] = config[0].spectacle.contacts.all()
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def response_add(self, request, obj, post_url_continue=None):
        if "add-to-spectacle" in request.POST and \
                request.POST['add-to-spectacle'] == "on":
            config = GlobalConfig.objects.all()
            if len(config) > 0 and config[0].spectacle is not None:
                config[0].spectacle.groupes.add(obj)
                config[0].spectacle.save()
        return super().response_add(request, obj)


@admin.register(Texteur)
class TexteurAdmin(admin.ModelAdmin):
    list_display = ('nom', 'actif')
    list_filter = (IsActif,)

    add_form_template = "admin/spectacle_elem_addform.html"

    def response_add(self, request, obj, post_url_continue=None):
        if "add-to-spectacle" in request.POST and \
                request.POST['add-to-spectacle'] == "on":
            config = GlobalConfig.objects.all()
            if len(config) > 0 and config[0].spectacle is not None:
                config[0].spectacle.texteurs.add(obj)
                config[0].spectacle.save()
        return super().response_add(request, obj)


@admin.register(Spectacle)
class SpectacleAdmin(admin.ModelAdmin):
    sortable_by = ('actif', 'nom', 'datetime')
    fields = ('nom', 'datetime', 'texteurs', 'contacts', 'groupes', 'modelsSMS')
    ordering = ('-datetime',)
    list_display = ('nom', 'datetime', 'actif')
    filter_horizontal = ('texteurs', 'contacts', 'groupes', 'modelsSMS')
    list_filter = (IsActif, 'datetime')


@admin.register(GlobalConfig)
class GlobalConfigAdmin(admin.ModelAdmin):
    list_display = ('spectacle', 'envoiSMS', 'receptionSMS', 'vmn',
                    'delaisFroid', 'delaisReception', 'userAPI')

    def has_add_permission(self, request):
        return GlobalConfig.objects.all().count() == 0


@admin.register(ModelSMS)
class ModelSMSAdmin(admin.ModelAdmin):
    fields = ('nom', 'Fcontent', 'Hcontent', 'Acontent', 'conversation', 'groupes')
    list_display = (
    'nom', 'Fcontent', 'Hcontent', 'Acontent', 'actif', 'conversation', 'groupes')
    list_display_links = ('nom', 'Fcontent', 'Hcontent', 'Acontent')
    list_filter = ('conversation', 'groupes', IsActif)
    list_editable = ('conversation', 'groupes')
    sortable_by = ('nom', 'conversation', 'groupes', 'actif')
    search_fields = ('nom', 'Hcontent', 'Fcontent', 'Acontent')

    add_form_template = "admin/spectacle_elem_addform.html"
    change_form_template = "admin/spectacle_elem_addform.html"

    def response_add(self, request, obj, post_url_continue=None):
        if "add-to-spectacle" in request.POST and \
                request.POST['add-to-spectacle'] == "on":
            config = GlobalConfig.objects.all()
            if len(config) > 0 and config[0].spectacle is not None:
                config[0].spectacle.modelsSMS.add(obj)
                config[0].spectacle.save()
        return super().response_add(request, obj)

# admin.site.register(GlobalConfig, GlobalConfigAdmin)
# admin.site.register(Spectacle)
# admin.site.register(Texteur)
# admin.site.register(Contact, ContactAdmin)
# admin.site.register(ModelSMS)
# admin.site.register(SMS, SMSAdmin)
# Register your models here.
