let timezone = undefined; // 'fr-FR'

let convSocket = new WebSocket(
	'ws://' + window.location.host +
	'/ws/conv/' + texteur + '/');

convSocket.onopen = function (e) {
	ws_send_getContactList();
};


function ws_send_getContactList() {
	convSocket.send(JSON.stringify({
		'type': "getcontactlist",
		'data': {'texteur_id': texteur_id}
	}));
}

function ws_send_sendmsg(message) {
	console.log("(ws)[send] sendsms : ["+message+"]");
	convSocket.send(JSON.stringify({
		'type': "sendmsg",
		'data': {
			'message': message,
			'contact_id': contact_id
		}
	}));
}

function ws_send_getcontactinfo(id) {
	convSocket.send(JSON.stringify({
		'type': "getcontactinfo",
		'data': {'contact_id': id}
	}));
}

function ws_send_msgseen() {
	convSocket.send(JSON.stringify({
		'type': "msgseen",
		'data': {'contact_id': contact_id}
	}));
}

function ws_recv_contactlist(data) {
	createContactList(data['contact_list']);
	if (contact_id === null) {
		if (contact_id = data['contact_list'].length === 0){
			return;
		}
		contact_id = data['contact_list'][0].pk;
		selectContact(contact_id);
	}else if(document.getElementById("conv_messages") === null){
		selectContact(contact_id);
	}
}

function ws_recv_contactinfo(data) {
	let contact = data['contact'][0];
	let messages = data['messages'];
	createConversation(contact.fields, messages);
}

function ws_recv_newmsg(data) {
	let id = data['contact_id'];
	let sms = data['sms'][0].fields;
	console.log("(ws)[recv] newmsg : ("+id+") ["+sms['message']+"]");
	let elem = document.getElementById("contact_button_" + id);
	let sender = sms['emitter'];
	let datetime = sms['datetime'];
	if ( sender === "" ){
		elem.dataset.lastmsgrecvtime = datetime;
	}else{
		elem.dataset.lastmsgsendtime = datetime;
	}
	console.log("get msg from contact "+id+", global contact id "+contact_id);
	if (contact_id == id) {
		let message = sms['message'];
		let smsDiv = createMessageDiv(message, datetime, sender);
		document.getElementById("conv_messages_wrapper").appendChild(smsDiv);

		// setContactFirst(document.getElementById("contact_button_" + id));
		ws_send_msgseen();
		scroll_messages_box();
	} else {
		// It's from a hidden contact
		signalNewMessage(id);
	}
	orderContact(elem);
}

function scroll_messages_box() {
	let messages_box = document.getElementById("conv_messages");
	messages_box.scrollTo(0, messages_box.scrollHeight);
}

function createContactListElem(contact, id) {
	let elem = document.getElementById('template-contact').content.cloneNode(true);
	let conv_contact = elem.querySelector('.conv_contact');

	elem.querySelector('.conv_contact_name').innerHTML =
		contact.prenom + " " + contact.nom;
	conv_contact.dataset.lastmsgrecvtime = contact.lastmsgrecvtime;
	conv_contact.dataset.lastmsgsendtime = contact.lastmsgsendtime;
	conv_contact.dataset.id = id;
	if (contact_id === id){
		conv_contact.classList.add('active');
	}
	conv_contact.id = "contact_button_" + id;
	conv_contact.onclick = function () {
		selectContact(id);
	};
	if (contact.newmessages > 0) {
		conv_contact.classList.add('newmessages');
		conv_contact.querySelector('.messages_n').innerHTML = contact.newmessages;
	}
	conv_contact.onkeydown = function(e) {
		navigateInContacts(this, e);
	};
	conv_contact.onkeyup = function(e){
		if(e.key === "Enter"){
			e.stopPropagation();
			e.stopImmediatePropagation();
			e.preventDefault();
			console.log("SELECT CONTACT : "+this.dataset.id);
			selectContact(this.dataset.id);
		}
	};
	return conv_contact;
}

function createContactList(contact_list) {
	contact_list_prio.innerHTML = "";
	contact_list_ras.innerHTML = "";
	for (let i = 0; i < contact_list.length; i++) {
		let elem = createContactListElem(
			contact_list[i].fields, contact_list[i].pk);
		orderContact(elem, true);
	}
}

function selectContact(selected_id) {
	let current_active = contact_list_box.querySelector(".active");
	let new_active = document.getElementById("contact_button_" + selected_id);
	let id = contact_id;
	contact_id = selected_id;

	if (current_active !== null) {
		current_active.classList.remove('active');
		current_active.onclick = function () {
			selectContact(id);
		};
	}
	if (new_active !== null) {
		new_active.classList.add('active');
		new_active.classList.remove('newmessages');
		new_active.querySelector('.messages_n').innerHTML = '';
		new_active.onclick = null;
	} else {
		console.error("selectContact : contact_id unfound !");
	}
	ws_send_getcontactinfo(selected_id);
}

function sendSMS() {
		let messageInputDom = document.querySelector('#conv-message-writer');
		let message = messageInputDom.value;
		ws_send_sendmsg(message);
		messageInputDom.value = '';
		messageInputDom.dispatchEvent(new Event("change"));
		messageInputDom.focus();
}

function navigateInContacts(current, e){
	if( e.ctrlKey){
		if ( e.key === "ArrowDown"){
			let elem = current.nextSibling;
			if (elem === null){
				if(current.parentNode.id === "list_prio"){
					elem = document.getElementById("list_ras").firstChild;
				}
				if(elem === null){
					elem = document.getElementById("list_prio").firstChild;
				}
				if(elem === null){
					elem = document.getElementById("list_ras").firstChild;
				}
			}
			elem.focus();
		}else if( e.key === "ArrowUp"){
			let elem = current.previousSibling;
			if (elem === null){
				if(current.parentNode.id === "list_ras"){
					elem = document.getElementById("list_prio").lastChild;
				}
				if(elem === null){
					elem = document.getElementById("list_ras").lastChild;
				}
				if(elem === null){
					elem = document.getElementById("list_prio").lastChild;
				}
			}
			elem.focus();
		}
	}
}

function createConversation(contact, messages) {
	let conv = document.getElementById('template-conversation').content.cloneNode(true);
	conv.querySelector('.prenom').innerHTML = contact.prenom;
	conv.querySelector('.nom').innerHTML = contact.nom;
	conv.querySelector('.genre').classList.add("genre_" + contact.genre);
	conv.querySelector('.genre').src = "/static/img/genre_" + contact.genre + ".png";
	conv.querySelector('.tel').innerHTML = "(" + contact.numero + ")";
	conv.querySelector('.swap').onclick = prepareAndOpenSwapModal;
	conv.querySelector('.eject').onclick = function e() {
		prepareAndOpenEjectModal(contact);
	};
	document.getElementById("conversation").innerHTML = '';
	document.getElementById("conversation").appendChild(conv);

	document.querySelector('#conv-message-writer').focus();
	document.querySelector('#conv-message-writer').onkeyup = function (e) {
		if (e.key === "Enter" && !e.shiftKey) {  // enter, return
			document.querySelector('#conv-message-writer').value =
				document.querySelector('#conv-message-writer').value.replace(
					/\n$/, ""
				);
			document.querySelector('#conv-message-sender').click();
		} else if (e.ctrlKey && e.altKey && e.key === "ArrowDown") {
			let elem = document.getElementById("list_prio").firstChild;
			if (elem === null) {
				elem = document.getElementById("list_ras").firstChild;
			}
			if (elem !== null) {
				elem.focus();
			}
		} else if (e.ctrlKey && e.shiftKey && e.key === "ArrowUp") {
			let elem = document.getElementById("list_ras").lastChild;
			if (elem === null) {
				elem = document.getElementById("list_prio").lastChild;
			}
			if (elem !== null) {
				elem.focus();
			}
		} else if (e.ctrlKey && (e.key === "ArrowDown" || e.key === "ArrowUp")) {
			navigateInContacts(document.querySelector(".active"), e);
		}
	};

	document.querySelector('#conv-message-sender').onclick = sendSMS;

	let msg_box = document.getElementById("conv_messages_wrapper");
	for (let i = 0; i < messages.length; i++) {
		msg_box.appendChild(createMessageDiv(
			messages[i].fields['message'],
			messages[i].fields['datetime'],
			messages[i].fields['emitter']
		));
	}

	initSMSTextareaCounter(document.getElementById("conv-message-writer"));
	ws_send_msgseen();
	scroll_messages_box();
	document.querySelector('#conv-message-writer').focus();
	fillPremsgModal(contact.genre);
}

function createMessageDiv(message, datetime, sender) {
	let sms = document.getElementById('template-sms').content.cloneNode(true);
	sms.querySelector('.sms-txt').innerHTML = message.replace(/\n/g, "<br />");
	let date = new Date(datetime);
	sms.querySelector('.sms-info').innerHTML = date.toLocaleTimeString(timezone);
	// sms.querySelector('.sms-info').innerHTML = date.toLocaleDateString(
	// 	timezone, {
	// 	'day': 'numeric',
	// 	'month': 'long',
	// 	'year': 'numeric',
	// }) + " " + date.toLocaleTimeString(timezone);
	if (sender !== null && sender !== "") {
		sms.querySelector('.sms').classList.add('texteur');
		sms.querySelector('.sms-info').innerHTML += " - " + sender;
	} else {
		sms.querySelector('.sms').classList.add('sender');
	}
	return sms;
}

// function setContactFirst(contact) {
// 	let parent = contact.parentNode;
// 	parent.removeChild(contact);
// 	parent.insertBefore(contact, parent.firstChild);
// }

function orderContact(contact, is_new) {
	let date_recv = new Date(contact.dataset.lastmsgrecvtime);
	let date_send = new Date(contact.dataset.lastmsgsendtime);
	// console.log("Order contact : "+contact.id);
	if (date_send < date_recv){
		if(!contact.classList.contains('chaud')){
			// console.log("CHAUD");
			contact.classList.add('chaud');
			contact.parentNode.removeChild(contact);
			contact_list_prio.appendChild(contact);
		}
	}else if(date_recv < time_limit && date_send < time_limit){
		if(!contact.classList.contains('froid')) {
			// console.log("FROID");
			contact.classList.add('froid');
			contact.parentNode.removeChild(contact);
			contact_list_prio.appendChild(contact);
		}
	}else{
		// console.log("RAS");
		if ( contact.classList.contains('chaud') ||
			contact.classList.contains('froid') || is_new === true) {
			contact.classList.remove('chaud');
			contact.classList.remove('froid');
			contact.parentNode.removeChild(contact);
			contact_list_ras.appendChild(contact);
		}
	}
}

function timerContactListOrder(){
	time_limit.setMinutes(time_limit.getMinutes() + 1);
	let contacts = contact_list_ras.children;
	for (let i = 0; i < contacts.length; i++){
		orderContact(contacts[i]);
	}
	contacts = contact_list_prio.children;
	for (let i = 0; i < contacts.length; i++){
		orderContact(contacts[i]);
	}
}

function signalNewMessage(id) {
	let contact = document.getElementById("contact_button_" + id);
	if (contact === null) {
		// newmessage from an unknown contact
		ws_send_getContactList();
		return
	}
	contact.classList.add("newmessages");
	let newmsg = contact.querySelector('.messages_n');
	let n = null;
	if (newmsg.innerHTML === "") {
		n = 0
	} else {
		n = parseInt(newmsg.innerHTML);
		console.log("n_message = " + n)
	}
	newmsg.innerHTML = n + 1;
	orderContact(contact);
}

function prepareAndOpenSwapModal(){
	let name_elem = document.getElementById("contact_name");
	let contatc_name = name_elem.querySelector('.prenom').innerHTML + " " +
		name_elem.querySelector('.nom').innerHTML;
	openSwapModal(contatc_name, contact_id, "conversation");
}

function prepareAndOpenEjectModal(contact) {
	console.log("prepare and open eject modal");
	document.getElementById("modal-eject-name").innerHTML =
		contact.prenom + " " + contact.nom.toUpperCase();
	document.getElementById("modal-eject-confirm").href =
		"/eject?contact_id="+contact_id+"&return_to=conversation";
	document.getElementById("modal-confirm-control").checked = true;
}

function getModelContent(selected, contact_genre){
	let model = null;
	for (let i = 0; i < model_list.length; i++) {
		if (selected === model_list[i].pk){
			model = model_list[i];
			break;
		}
	}
	if (contact_genre === "H") {
		return model.fields['Hcontent'];
	}else if(contact_genre === "F"){
		return model.fields['Fcontent'];
	}else{
		return model.fields['Acontent'];
	}
}

function fillPremsgModal(contact_genre){
	let model_select = document.getElementById("form-premsg-model");
	model_select.innerText = "";
	for (let i = 0; i < model_list.length; i++) {
		let model = document.createElement("option");
		model.value = model_list[i].pk;
		// let content = "";
		// if (contact_genre !== "F") {
		// 	content = model_list[i].fields['Hcontent'];
		// }else{
		// 	content = model_list[i].fields['Fcontent'];
		// }
		// if (content.length > 30){
		// 	content = content.slice(0, 17) + " [...] " + content.slice(-8, content.length);
		// }
		model.innerHTML = model_list[i].fields['nom'];
		model_select.appendChild(model);
	}
	model_select.onchange = function() {
		let selected = parseInt(this.options[this.selectedIndex].value);
		document.getElementById("form-premsg-show").innerHTML =
			getModelContent(selected, contact_genre);
		document.getElementById("form-premsg-show").dispatchEvent(new Event("change"));
	};
	initSMSTextareaCounter(document.getElementById("form-premsg-show"));

	document.getElementById("form-premsg-send").onclick = function (e) {
		let model_select = document.getElementById("form-premsg-model");
		let selected = parseInt(model_select.options[model_select.selectedIndex].value);
		document.querySelector('#conv-message-writer').value = getModelContent(selected, contact_genre);
		sendSMS();
		document.getElementById("modal-premsg-control").checked = false;
	};

	document.getElementById("form-premsg-ok").onclick = function (e) {
		let model_select = document.getElementById("form-premsg-model");
		let selected = parseInt(model_select.options[model_select.selectedIndex].value);
		document.querySelector('#conv-message-writer').value = getModelContent(selected, contact_genre);
		document.querySelector('#conv-message-writer').dispatchEvent(new Event('change'));
		document.getElementById("modal-premsg-control").checked = false;
	};

	model_select.dispatchEvent(new Event('change'));
}

function openPremsgModal(){
	document.getElementById("modal-premsg-control").checked = true;
}

convSocket.onmessage = function (e) {
//	console.log(e.data);
	let message = JSON.parse(e.data);
	console.log(message);
	if (message['type'] === "contactlist") {
		ws_recv_contactlist(message['data']);
	} else if (message['type'] === "newmsg") {
		ws_recv_newmsg(message["data"]);
	} else if (message['type'] === "contactinfo") {
		ws_recv_contactinfo(message["data"]);
	} else {
		console.warn("discard message : " + message);
	}
};

convSocket.onclose = function (e) {
	console.error('Chat socket closed unexpectedly');
};

