function swap(contact_id) {
	alert("Swap contact : " + contact_id);
}


function openSwapModal(contact_name, contact_id, return_to) {
	document.getElementById("form-swap-contact").value = contact_name;
	document.getElementById("form-swap-contact-id").value = contact_id;
	document.getElementById("form-swap-texteur-id").value = texteur_id;
	let texteur_select = document.getElementById("form-swap-texteur");
	texteur_select.innerText = "";
	for (let i = 0; i < texteur_list.length; i++) {
		let texteur = document.createElement("option");
		texteur.value = texteur_list[i].pk;
		texteur.innerHTML = texteur_list[i].fields['nom'];
		texteur_select.appendChild(texteur);
	}
	document.getElementById("form-swap-return-to").value = return_to;
	document.getElementById("modal-swap-control").checked = true;
}


function computeSMSlen(text) {
	let n_char = text.length;
	let limit_sms = 160;
	let unicode = regex_gsm.test(text);
	if (unicode !== false) {
		limit_sms = 70;
	}
	if (n_char > limit_sms) {
		limit_sms -= 7;
	}
	let n_sms = Math.floor(n_char / limit_sms) + 1;
	let remain = ( limit_sms * n_sms ) - n_char;
	return {char: n_char, n_sms: n_sms, remain: remain, unicode: unicode};
}

function initSMSTextareaCounter(textarea) {

	console.log("init textarea : "+textarea);
	//the div to contain the background and the textarea
	let div = document.getElementById(textarea.id + "-counter");
	console.log(div);

	let counter = div.querySelector(".char-counter-background");
	console.log(counter);

	function eventSMSTextareaCounter() {
		let len = computeSMSlen(this.value);
		counter.innerHTML = len.remain + "/" + len.n_sms;
	}

	textarea.addEventListener("keyup", eventSMSTextareaCounter, false);
	textarea.addEventListener("change", eventSMSTextareaCounter, false);



	textarea.parentNode.removeChild(textarea);
	div.appendChild(textarea);
}

