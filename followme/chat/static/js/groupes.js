let model_select = document.getElementById('groupe-model');

model_select.onchange = function(){
	updateModelPreview(
		parseInt(this.options[this.selectedIndex].value)
	);
};

function updateModelPreview(selected){
	let model = null;
	for (let i = 0; i < model_list.length; i++) {
		if (selected === model_list[i].pk){
			model = model_list[i];
			break;
		}
	}
	document.getElementById('groupe-show-Fcontent').value =
		model.fields['Fcontent'];
	document.getElementById('groupe-show-Fcontent').dispatchEvent(new Event("change"));
	document.getElementById('groupe-show-Hcontent').value =
		model.fields['Hcontent'];
	document.getElementById('groupe-show-Hcontent').dispatchEvent(new Event("change"));
	document.getElementById('groupe-show-Acontent').value =
		model.fields['Acontent'];
	document.getElementById('groupe-show-Acontent').dispatchEvent(new Event("change"));
}

function showConfirm(){
	document.getElementById("modal-confirm-control").checked = true;
}